//Reverse a number
#include <stdio.h>
int main()
{

    int i=0 ,n ; //n= number,i = reverse
    printf ("Enter a number: ");
    scanf ("%d", &n);

    while (n != 0)
        {
           i = i * 10;
           i = i + n%10;
           n = n/10;
        }
        printf ( "Reverse number = %d\n" , i);

        return 0 ;
}
